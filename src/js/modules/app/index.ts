import { not, isNil } from 'ramda';
import { runServiceWorkerLauncher } from './services';
import { preloader } from '../../components';

const preloaderChecker = (): void => {
  const htmlPreloaderElem: HTMLElement | undefined = document.querySelector('.preloader');
  if (not(isNil(htmlPreloaderElem))) {
    setTimeout(preloader.disable, 2000);
  }
};

function runAppModule() {
  runServiceWorkerLauncher();
  preloaderChecker();
}

__.thenLoadPage(runAppModule);
