import { not, and, equals } from 'ramda';
import { Workbox } from 'workbox-window';

export default function runServiceWorkerLauncher(): void {
  const isProduction = equals(NODE_ENV, 'production');

  if (and('serviceWorker' in navigator, isProduction)) {
    const wb = new Workbox('./service-worker.js');

    wb.addEventListener('activated', (event) => {
      // `event.isUpdate` will be true if another version of the service
      // worker was controlling the page when this version was registered.
      if (not(event.isUpdate)) {
        console.log('Service worker activated for the first time!');

        // If your service worker is configured to precache assets, those
        // assets should all be available now.
      }
    });

    wb.addEventListener('waiting', () => {
      // `event.wasWaitingBeforeRegister` will be false if this is
      // the first time the updated service worker is waiting.
      // When `event.wasWaitingBeforeRegister` is true, a previously
      // updated service worker is still waiting.
      // You may want to customize the UI prompt accordingly.

      // Assumes your app has some sort of prompt UI element
      // that a user can either accept or reject.

      const answer = confirm('Update service worker to the latest version?');

      if (answer) {
        // Assuming the user accepted the update, set up a listener
        // that will reload the page as soon as the previously waiting
        // service worker has taken control.
        wb.addEventListener('controlling', () => {
          window.location.reload();
        });
        wb.messageSW({ type: 'SKIP_WAITING' });
      }
    });

    // Register the service worker after event listeners have been added.
    wb.register();
  }
}
