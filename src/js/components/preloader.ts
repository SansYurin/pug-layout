const preloader = {
  disable(): void {
    document.body.classList.add('loaded');
  },
};

export default preloader;
