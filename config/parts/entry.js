module.exports = function createPart(options) {
  return {
    vendors: ['./src/scss/vendors/_index.scss'],
    app: ['./src/js/modules/app/index.ts', './src/scss/modules/app/_index.scss'],
    ...options,
  };
};
