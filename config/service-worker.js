import { equals, lensPath, view } from 'ramda';
import { precacheAndRoute } from 'workbox-precaching/precacheAndRoute';

precacheAndRoute(self.__WB_MANIFEST);

self.addEventListener('message', (event) => {
  const typeOf = lensPath(['data', 'type']);
  const isWaiting = equals(view(typeOf, event), 'SKIP_WAITING');
  if (isWaiting) {
    self.skipWaiting();
  }
});