/*
 * workbox-webpack-plugin
 *
 * Workbox provides two webpack plugins: one that generates a complete service worker for you
 * and one that generates a list of assets to precache that is injected into a service worker file.
 * The plugins are implemented as two classes in the workbox-webpack-plugin module,
 * named GenerateSW and InjectManifest.
 * The answers to the following questions can help you choose the right plugin and configuration to use.
 *
 * @see https://developers.google.com/web/tools/workbox/modules/workbox-webpack-plugin
 *
 * InjectManifest
 * The InjectManifest plugin will generate a list of URLs to precache and
 * add that precache manifest to an existing service worker file.
 * It will otherwise leave the file as-is.
 * @see https://developers.google.com/web/tools/workbox/reference-docs/latest/module-workbox-webpack-plugin.InjectManifest
 */

const path = require('path');
const { InjectManifest } = require('workbox-webpack-plugin');

const defaultInjectManifestOptions = {
  swSrc: path.resolve(__dirname, '../service-worker.js'),
  swDest: 'service-worker.js',
  exclude: [
    /\.map$/,
    /manifest$/,
    /\.htaccess$/,
    /service-worker\.js$/,
    /sw\.js$/,
  ],
};

module.exports = function createPlugin(options) {
  return new InjectManifest({
    ...defaultInjectManifestOptions,
    ...options
  });
};